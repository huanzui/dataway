# dataway
#### 项目介绍
Dataway 是基于 DataQL 服务聚合能力，为应用提供的一个接口配置工具。使得使用者无需开发任何代码就配置一个满足需求的接口。整个接口配置、测试、冒烟、发布。一站式都通过 Dataway 提供的 UI 界面完成。UI 会以 Jar 包方式提供并集成到应用中并和应用共享同一个 http 端口，应用无需单独为 Dataway 开辟新的管理端口。

######
本示例旨在对dataway的一个初级学习使用更多操作和使用手册可以参考下面官方文档

####Dataway 官方手册
######https://www.hasor.net/web/dataway/about.html

####DataQL手册地址
######https://www.hasor.net/web/dataql/what_is_dataql.html